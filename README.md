# Items List

Show a list of items getted from an API.

# Arquitecture pattern selected

# MVVM 
Allows me to encapsulate and get a high cohesion and low coupling. If the case was that I have to modify the raw data that I get from the API to show it in the view MVVM is much more usefull.

# Networking
I generate a Singleton service class that handles all the network aspects.

# Communication
I use a closure from the APICli to tell when the networking is over. Can use notifications/observers in the case that whant to do something else with the UI. I use property observers to notify the table view cells that the data is loaded.

# Tests
Implemented a simple XCTest to evaluate the model and the view model properties.