//
//  gl_items_listTests.swift
//  gl-items-listTests
//
//  Created by Federico Frias on 30/10/2019.
//  Copyright © 2019 ffrias. All rights reserved.
//

import XCTest
@testable import gl_items_list


class gl_items_listTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testDataViewModel(){
        let dataMock = DataModel(title: "test", description: "some description for the test.", imagePath: "https://picsum.photos/100/100?image=0")
        let dataViewModelMock = DataViewModel(data: dataMock)
        
        XCTAssertEqual(dataMock.title, dataViewModelMock.title)
        
        XCTAssertEqual(dataMock.description, dataViewModelMock.description)
        
        XCTAssertEqual(dataMock.imagePath, dataViewModelMock.image)
    }

}
