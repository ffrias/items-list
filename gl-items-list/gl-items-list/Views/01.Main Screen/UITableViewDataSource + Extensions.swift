//
//  UITableViewDataSource + Extensions.swift
//  gl-items-list
//
//  Created by Federico Frias on 29/10/2019.
//  Copyright © 2019 ffrias. All rights reserved.
//

import Foundation
import UIKit

extension ViewController:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = table.dequeueReusableCell(withIdentifier: "MainTableViewCell") as! MainTableViewCell
        cell.dataViewModel = dataList[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dataToSend = dataList[indexPath.row]
        performSegue(withIdentifier: "loadDetailScreen", sender: dataToSend)
    }
    
}
