//
//  ViewController.swift
//  gl-items-list
//
//  Created by Federico Frias on 28/10/2019.
//  Copyright © 2019 ffrias. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var table: UITableView!
    var dataList = [DataViewModel]()

    override func viewDidLoad() {
        super.viewDidLoad()
        table.delegate = self
        table.dataSource = self
        loadData()
    }
    
    func loadData(){
        APICli.sharedInstance.getDataFromApi({ (dataModel, err) in
            if let err = err{
                print("Failed to fetch data: ", err)
                return
            }
            self.dataList = dataModel?.map({return DataViewModel(data: $0)}) ?? []
            self.table.reloadData()
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "loadDetailScreen"{
            let destVC = segue.destination as! DetailViewController
            destVC.data = sender as? DataViewModel
        }
    }
}
