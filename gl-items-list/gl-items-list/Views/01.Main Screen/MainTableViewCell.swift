//
//  MainTableViewCell.swift
//  gl-items-list
//
//  Created by Federico Frias on 29/10/2019.
//  Copyright © 2019 ffrias. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class MainTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var dataImage: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var dataViewModel: DataViewModel!{
        didSet{
            titleLabel?.text = dataViewModel.title
            descriptionLabel?.text = dataViewModel.description
            
           if let url = URL(string: dataViewModel.image){
                let URLRequest = Foundation.URLRequest(url:url)
                let imageCache = AutoPurgingImageCache()
                
                dataImage!.af_setImage(
                    withURL: url,
                    placeholderImage: UIImage(named: "image_placeholder"),
                    filter: nil,
                    imageTransition: .crossDissolve(0.5),
                    completion: { response in
                        if (response.result.isSuccess){
                            
                            imageCache.add(response.result.value!, for: URLRequest, withIdentifier: self.dataViewModel.title)
                        }else{
                            print("Error: " + String(describing: response.result.error))
                        }
                    }
                )
            }
       }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        let imageV = UIImageView(image: UIImage(named: "image_placeholder"))
        self.dataImage.image = imageV.image
        
        titleLabel.text = ""
        descriptionLabel.text = ""
    }
}
