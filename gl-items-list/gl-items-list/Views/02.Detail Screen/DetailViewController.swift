//
//  DetailViewController.swift
//  gl-items-list
//
//  Created by Federico Frias on 29/10/2019.
//  Copyright © 2019 ffrias. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class DetailViewController: UIViewController {
    
    @IBOutlet weak var detailImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var data: DataViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }
    
    func setUI(){
        titleLabel.text = data?.title
        descriptionLabel.text = data?.description
        
        if let url = URL(string: (data?.image)!){
            let URLRequest = Foundation.URLRequest(url:url)
            let imageCache = AutoPurgingImageCache()
            
            
            detailImage!.af_setImage(
                withURL: url,
                placeholderImage: UIImage(named: "image_placeholder"),
                filter: nil,
                imageTransition: .crossDissolve(0.5),
                completion: { response in
                    if (response.result.isSuccess){
                        
                        imageCache.add(response.result.value!, for: URLRequest, withIdentifier: self.data?.title)
                        
                        print ("image fetched ok")
                    }else{
                        print("Error: " + String(describing: response.result.error))
                    }
            }
            )
        }
    }
}
