//
//  dataModel.swift
//  gl-items-list
//
//  Created by Federico Frias on 29/10/2019.
//  Copyright © 2019 ffrias. All rights reserved.
//

import Foundation


struct DataModel {
    var title:String
    var description:String
    var imagePath:String
}
