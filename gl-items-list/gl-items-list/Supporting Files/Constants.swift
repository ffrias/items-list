//
//  Constants.swift
//  gl-items-list
//
//  Created by Federico Frias on 29/10/2019.
//  Copyright © 2019 ffrias. All rights reserved.
//

import Foundation

struct Constants {
    struct API {
        static let BaseUrl = "https://private-f0eea-mobilegllatam.apiary-mock.com"
        static let list = "/list"
    }
}
