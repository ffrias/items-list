//
//  APICli.swift
//  gl-items-list
//
//  Created by Federico Frias on 29/10/2019.
//  Copyright © 2019 ffrias. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class APICli{
    static let sharedInstance = APICli()
    fileprivate init(){}
    
    func getDataFromApi(_ completion:@escaping ([DataModel]?,Error?)->()){
        
        let base_url = Constants.API.BaseUrl
        let urn = Constants.API.list
        
        Alamofire.request(base_url + urn, method: .get)
            .validate()
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        var dataArray = [DataModel]()
                        let json = JSON(data)
                        
                        for result in json.arrayValue{
                            let title = result["title"].stringValue
                            let description = result["description"].stringValue
                            let imagePath = result["image"].stringValue
                            
                            let dataFetched = DataModel.init(title: title, description: description, imagePath: imagePath)
                            
                            dataArray.append(dataFetched)
                        }
                        completion(dataArray, nil)
                        
                        //TO DO: We can add a notification here to let the view that the fetching is
                        //done and for example dismiss a load spinner.
                        
                        print("Data from API fetched OK.")
                    }
                case .failure(let error):
                    completion(nil, error)
                    print("Error: \(error)")
                }
        }
    }
}
