//
//  dataViewModel.swift
//  gl-items-list
//
//  Created by Federico Frias on 29/10/2019.
//  Copyright © 2019 ffrias. All rights reserved.
//
//
//We can use this View Model to transform the raw data of dataModel to a formatted readable data.
//For example, if we have here a date we can transform into a format selected from the user. If we
//have colors or backgrounds or custom messages deppending on the value of the raw data we can handle
//it here too.

import Foundation
import UIKit
import AlamofireImage

class DataViewModel{
    
    let title: String
    let description: String
    var image: String
    
    init(data: DataModel){
        self.title = data.title
        self.description = data.description
        self.image = data.imagePath
    }
}
